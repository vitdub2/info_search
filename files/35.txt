<div id="post-content-body">
 <div>
  <div class="article-formatted-body article-formatted-body article-formatted-body_version-2">
   <div xmlns="http://www.w3.org/1999/xhtml">
    <figure class="full-width">
     <img data-blurred="true" data-src="https://habrastorage.org/getpro/habr/upload_files/33a/780/9b1/33a7809b1a97e18aeb64136efd4075e1.jpg" height="4160" src="https://habrastorage.org/r/w780q1/getpro/habr/upload_files/33a/780/9b1/33a7809b1a97e18aeb64136efd4075e1.jpg" width="3120"/>
     <figcaption>
     </figcaption>
    </figure>
    <p>
     56K Modem — довольно редкий представитель связной периферии для старых КПК.  Подробнее о ней в целом, можно узнать, например,
     <a href="https://4pda.to/forum/index.php?showtopic=42116" rel="noopener noreferrer nofollow">
      здесь
     </a>
    </p>
    <p>
     Но если WI-FI и Bluetooth карточки у нас довольно часто еще встречаются в продаже, то модемы 56K как-то не очень. Мое предположение: они не успели получить мало-мальски массовых продаж. К моменту появления на рынке отработанных и доступных по цене для простого Васи Пупкина моделей, технология dial-up безвозвратно ушла в закат.
    </p>
    <p>
     По крайней мере, в моих условиях (сибирский городок на 100 тыс. населения) уже с 2005 года в домовладениях (а в организациях - еще раньше) стали повсеместно распространяться выделенные линии с более высокими (на порядки) скоростями и уже более-менее гуманными тарифами на безлимит (а не объем трафика, как ранее). Примерно тогда же стал доступен мобильный GPRS, а чуть позже — и 3G. Ну, а еще, через пару-тройку лет, в каждую квартиру начали проникать оптические GPON-щупальца телеком-гигантов.
    </p>
    <p>
     На фотках в интернете модем кажется большим, в действительности — весьма маленький
    </p>
    <figure class="full-width">
     <img data-blurred="true" data-src="https://habrastorage.org/getpro/habr/upload_files/c3f/320/152/c3f32015236da690b4bfd161e69403d2.jpg" height="3120" src="https://habrastorage.org/r/w780q1/getpro/habr/upload_files/c3f/320/152/c3f32015236da690b4bfd161e69403d2.jpg" width="4160"/>
     <figcaption>
     </figcaption>
    </figure>
    <p>
     Конкретно по моему экземпляру информации в интернете почти нет. Никакой технической документации найти не удалось. Сайт производителя вроде бы жив, но не знает об этом изделии. Из претендующих на подробность обзоров нашелся целый один,
     <a href="http://www.handy.ru/read/review/pretec.html" rel="noopener noreferrer nofollow">
      также 20-летней давности
     </a>
     .
    </p>
    <p>
     В комплекте, помимо модема, оказался PC-card переходник в футлярчике, немного макулатуры, в том числе брошюрка с инструкциями по настройке.
    </p>
    <figure class="full-width">
     <img data-blurred="true" data-src="https://habrastorage.org/getpro/habr/upload_files/d32/443/745/d32443745a982e4eed3beb0622a869ad.jpg" height="3120" src="https://habrastorage.org/r/w780q1/getpro/habr/upload_files/d32/443/745/d32443745a982e4eed3beb0622a869ad.jpg" width="4160"/>
     <figcaption>
     </figcaption>
    </figure>
    <p>
     Компакт-диск с ПО. Пользователю предлагается установить следующее:
    </p>
    <figure class="full-width">
     <img data-blurred="true" data-src="https://habrastorage.org/getpro/habr/upload_files/751/5dc/e09/7515dce097344ef69ee67757690a44b5.jpg" height="552" src="https://habrastorage.org/r/w780q1/getpro/habr/upload_files/751/5dc/e09/7515dce097344ef69ee67757690a44b5.jpg" width="835"/>
     <figcaption>
     </figcaption>
    </figure>
    <p>
     Продавец положил даже оригинал чека из магазина!
    </p>
    <figure class="full-width">
     <img data-blurred="true" data-src="https://habrastorage.org/getpro/habr/upload_files/b30/fd9/bd2/b30fd9bd208f2528a9e2b3221f03a213.jpg" height="3120" src="https://habrastorage.org/r/w780q1/getpro/habr/upload_files/b30/fd9/bd2/b30fd9bd208f2528a9e2b3221f03a213.jpg" width="4160"/>
     <figcaption>
     </figcaption>
    </figure>
    <p>
     2,5 тыс. рублей в 2004 году составляло примерно 1/5-1/7 моей зарплаты. Как для личной игрушки, тогда - дорого, да и не сильно-то нужно.
    </p>
    <p>
     Но если вы, например, деловой человек, гоняющий по просторам нашей необъятной начала «нулевых», которому нужна связь с офисом, переписка с контрагентами, и всё такое прочее — почему бы и нет.  В гостинице воткнулся в телефонную розетку, и вуаля — весь мир на экране.
    </p>
    <figure class="full-width">
     <img data-blurred="true" data-src="https://habrastorage.org/getpro/habr/upload_files/cbb/2e5/4af/cbb2e54af6c8037a2bba862d7e9257d0.jpg" height="3120" src="https://habrastorage.org/r/w780q1/getpro/habr/upload_files/cbb/2e5/4af/cbb2e54af6c8037a2bba862d7e9257d0.jpg" width="4160"/>
     <figcaption>
     </figcaption>
    </figure>
    <p>
     Обострение плюшкинизма в свое время заставило меня заиметь аж два таких КПК. Слева простой 620-й, у которого из внешних коммуникаций только ИК-порт.  Справа — версия 620ВТ, оснащенная блютузом.
    </p>
    <figure class="full-width">
     <img data-blurred="true" data-src="https://habrastorage.org/getpro/habr/upload_files/589/07a/39b/58907a39bba3815c7fe56d941b6e1417.jpg" height="3120" src="https://habrastorage.org/r/w780q1/getpro/habr/upload_files/589/07a/39b/58907a39bba3815c7fe56d941b6e1417.jpg" width="4160"/>
     <figcaption>
     </figcaption>
    </figure>
    <p>
     Левый был модернизирован неизвестным умельцем. Вместо штатного аккумулятора, самодельно установлен новый. Прибор показывает емкость около 1200 ма/ч, хватает на пару-тройку часов. На боку запилено дополнительное гнездо. Гаджетом активно пользовались: корпус люфтит, стилус не держится в гнезде, тачскрин иногда подглючивает.
    </p>
    <p>
     Правый на фото — тоже рабочий, в первозданном виде, и судя по состоянию, почти не пользовался. Только без штатного аккумулятора, который вздулся и был утилизирован. В итоге «подопытным кроликом» выбран левый.
    </p>
    <figure class="full-width">
     <img data-blurred="true" data-src="https://habrastorage.org/getpro/habr/upload_files/972/a1f/624/972a1f624821e57cbf4acbcdad11be14.jpg" height="3120" src="https://habrastorage.org/r/w780q1/getpro/habr/upload_files/972/a1f/624/972a1f624821e57cbf4acbcdad11be14.jpg" width="4160"/>
     <figcaption>
     </figcaption>
    </figure>
    <p>
     Раздающим dial-up устройством на ПК будет вот эта штука. Примерно
     <a href="https://aliexpress.ru/item/1005004900736892.html?sku_id=12000030953710581&amp;spm=a2g2w.productlist.search_results.2.123d4aa6JZ5mla" rel="noopener noreferrer nofollow">
      такой
     </a>
     , но
     <s>
      без перламутровых пуговиц
     </s>
     с одним портом RJ11.
    </p>
    <figure class="full-width">
     <img data-blurred="true" data-src="https://habrastorage.org/getpro/habr/upload_files/e25/015/c08/e25015c083ba7f7beb34406aceb86078.jpg" height="3120" src="https://habrastorage.org/r/w780q1/getpro/habr/upload_files/e25/015/c08/e25015c083ba7f7beb34406aceb86078.jpg" width="4160"/>
     <figcaption>
     </figcaption>
    </figure>
    <figure class="full-width">
     <img data-blurred="true" data-src="https://habrastorage.org/getpro/habr/upload_files/573/1ed/b21/5731edb214113c382e1590deefb9a3ec.jpg" height="3120" src="https://habrastorage.org/r/w780q1/getpro/habr/upload_files/573/1ed/b21/5731edb214113c382e1590deefb9a3ec.jpg" width="4160"/>
     <figcaption>
     </figcaption>
    </figure>
    <p>
     Кому интересно, так он устроен внутри
    </p>
    <figure class="full-width">
     <img data-blurred="true" data-src="https://habrastorage.org/getpro/habr/upload_files/b6c/096/c52/b6c096c523e16029c5dba8084f1d62f6.jpg" height="3072" src="https://habrastorage.org/r/w780q1/getpro/habr/upload_files/b6c/096/c52/b6c096c523e16029c5dba8084f1d62f6.jpg" width="4096"/>
     <figcaption>
     </figcaption>
    </figure>
    <figure class="full-width">
     <img data-blurred="true" data-src="https://habrastorage.org/getpro/habr/upload_files/ac2/666/95e/ac266695e0319df3bd2e227746a52996.jpg" height="3072" src="https://habrastorage.org/r/w780q1/getpro/habr/upload_files/ac2/666/95e/ac266695e0319df3bd2e227746a52996.jpg" width="4096"/>
     <figcaption>
     </figcaption>
    </figure>
    <p>
     ПК на Win7 (домашняя базовая) без проблем подхватил свисток, установил драйвер, устройство появилось в списке модемов, диагностику проходит, всё замечательно. КПК также легко подружился с модемом. Создаем на ПК входящее, а на КПК - исходящее подключение, и в целом производим их настройку по методам, описанным
     <a href="https://habr.com/ru/company/timeweb/blog/712448/" rel="noopener noreferrer nofollow">
      выше
     </a>
     и во многих других источниках.
    </p>
    <p>
     Первым делом надо заставить модемы в принципе соединиться и услышать друг друга. Большие надежды возлагались на простейшую эмуляцию телефонной линии по схеме
     <a href="https://habr.com/ru/company/ruvds/blog/518520/" rel="noopener noreferrer nofollow">
      отсюда
     </a>
    </p>
    <figure class="">
     <img data-src="https://habrastorage.org/getpro/habr/upload_files/42e/615/247/42e6152479c3c3e31f00c597104bbf93.png" height="176" src="https://habrastorage.org/r/w1560/getpro/habr/upload_files/42e/615/247/42e6152479c3c3e31f00c597104bbf93.png" width="495"/>
     <figcaption>
     </figcaption>
    </figure>
    <p>
     Колхозно-подручным методом собран «эмулятор». Который не заработал. Модемы исправно делали вызовы, но не слышали друг друга. Подумалось, что 9 вольт может быть мало, добавил последовательно вторую «Крону».
    </p>
    <figure class="full-width">
     <img data-blurred="true" data-src="https://habrastorage.org/getpro/habr/upload_files/fc6/d4e/349/fc6d4e34908cc8cf6dd0d17797a115bc.jpg" height="3120" src="https://habrastorage.org/r/w780q1/getpro/habr/upload_files/fc6/d4e/349/fc6d4e34908cc8cf6dd0d17797a115bc.jpg" width="4160"/>
     <figcaption>
     </figcaption>
    </figure>
    <p>
     Тоже нет. Попробовал воткнуть батарейку просто в разрыв одного из проводов линии. На форумах вычитал, что должно всё работать, народ вспоминал, что прекрасно с «кроной» в линии соединялись два «Курьера» из разных зданий.
    </p>
    <p>
     Но увы, не в моем случае. Видимо, оборудование требовало более достоверной среды передачи данных. Что же делать, покупать специально микро-АТС?
    </p>
    <p>
     Уже полез было на интернет-барахолку, но вспомнив про личные закрома Родины, на свет было извлечено это.
    </p>
    <figure class="full-width">
     <img data-blurred="true" data-src="https://habrastorage.org/getpro/habr/upload_files/2c3/41b/b24/2c341bb24c1825e2fe426fbd1d80fd05.jpg" height="3120" src="https://habrastorage.org/r/w780q1/getpro/habr/upload_files/2c3/41b/b24/2c341bb24c1825e2fe426fbd1d80fd05.jpg" width="4160"/>
     <figcaption>
     </figcaption>
    </figure>
    <figure class="full-width">
     <img data-blurred="true" data-src="https://habrastorage.org/getpro/habr/upload_files/3e0/b31/14d/3e0b3114da3aa2dfe49939bd85c1c439.jpg" height="4160" src="https://habrastorage.org/r/w780q1/getpro/habr/upload_files/3e0/b31/14d/3e0b3114da3aa2dfe49939bd85c1c439.jpg" width="3120"/>
     <figcaption>
     </figcaption>
    </figure>
    <p>
     Шлюз сброшен до дефолтных настроек, принимающий модем воткнут в 1 порт на номер по умолчанию 701, звонящий — во 2 порт на номер 702.  Ииииии.....
    </p>
    <figure class="full-width">
     <img data-blurred="true" data-src="https://habrastorage.org/getpro/habr/upload_files/51b/794/3d6/51b7943d60e49fde25627cdbab74d913.jpg" height="4160" src="https://habrastorage.org/r/w780q1/getpro/habr/upload_files/51b/794/3d6/51b7943d60e49fde25627cdbab74d913.jpg" width="3120"/>
     <figcaption>
     </figcaption>
    </figure>
    <p>
     Аллилуйя! Есть контакт! После третьего звонка USB-свисток на ПК поднял трубку и модемы начали общаться друг с другом.
    </p>
    <figure class="full-width">
     <img data-blurred="true" data-src="https://habrastorage.org/getpro/habr/upload_files/aa6/40e/e9a/aa640ee9acac5a36faf3e26023c4594e.jpg" height="3120" src="https://habrastorage.org/r/w780q1/getpro/habr/upload_files/aa6/40e/e9a/aa640ee9acac5a36faf3e26023c4594e.jpg" width="4160"/>
     <figcaption>
     </figcaption>
    </figure>
    <p>
     Входящее соединение установлено, появилась новая сеть RAS (dial-in) Interface.
    </p>
    <figure class="full-width">
     <img data-blurred="true" data-src="https://habrastorage.org/getpro/habr/upload_files/9fe/3fd/71a/9fe3fd71aaf38c890c90b85c2060d83e.jpg" height="3120" src="https://habrastorage.org/r/w780q1/getpro/habr/upload_files/9fe/3fd/71a/9fe3fd71aaf38c890c90b85c2060d83e.jpg" width="4160"/>
     <figcaption>
     </figcaption>
    </figure>
    <p>
     ПК - КПК успешно пингуются в обе стороны, физически связь есть. Но интернет на КПК не работает.
    </p>
    <p>
     Тут у меня случился фейл в том, чтобы с кавалерийского наскока заставить домашнюю «семерку» раздавать интернет через входящее подключение штатными средствами. То, что в ХР делается легко в несколько кликов, в «семерке» для меня оказалось нетривиальной задачей.
    </p>
    <p>
     Здесь надо оговориться, что автор и близко не имеет отношения к айти, а владение им ПК сводится к уровню «уверенный пользователь». После ознакомительного курения форумов выяснилось, что в моем случае проще доверится какой-нибудь специальной программе, коей была выбрана WinGate.
    </p>
    <p>
     И вот же колдунство, после этого всё заработало )
    </p>
    <figure class="full-width">
     <img data-blurred="true" data-src="https://habrastorage.org/getpro/habr/upload_files/cc8/deb/d43/cc8debd43b079f6829408e3f2f66908f.jpg" height="4160" src="https://habrastorage.org/r/w780q1/getpro/habr/upload_files/cc8/deb/d43/cc8debd43b079f6829408e3f2f66908f.jpg" width="3120"/>
     <figcaption>
     </figcaption>
    </figure>
    <p>
     Корректно отображается страница Яндекса, нормально работают сайты web 1.0 — Old-DOS,  dgmag.in, и другие. При этом диспетчер задач иной раз показывает совершенно фантастические для dial-up скорости сети RAS — более 100 кбит/с.
    </p>
    <figure class="full-width">
     <img data-blurred="true" data-src="https://habrastorage.org/getpro/habr/upload_files/aa1/573/37c/aa157337cb1169d4a943f1f019a69010.jpg" height="3120" src="https://habrastorage.org/r/w780q1/getpro/habr/upload_files/aa1/573/37c/aa157337cb1169d4a943f1f019a69010.jpg" width="4160"/>
     <figcaption>
     </figcaption>
    </figure>
    <p>
     Сходу не удалось найти «приборный» способ измерить истинную скорость интернет-соединения на КПК, пробуем эмпирическим путем.
    </p>
    <p>
     Файл .zip с журналом Downgrade за 2020 год объемом 6,82 мегабайта скачивался 40 минут 30 секунд, очень грубо прикинем: 6 820 000 байт /2430 сек. = 2 806,5 байт/сек. * 8 бит = 22,4 кбит/сек.
    </p>
    <figure class="full-width">
     <img data-blurred="true" data-src="https://habrastorage.org/getpro/habr/upload_files/6b3/de2/fb5/6b3de2fb551b7ac7a9c9bc5ec552d6fb.jpg" height="4160" src="https://habrastorage.org/r/w780q1/getpro/habr/upload_files/6b3/de2/fb5/6b3de2fb551b7ac7a9c9bc5ec552d6fb.jpg" width="3120"/>
     <figcaption>
     </figcaption>
    </figure>
    <p>
     А давайте попробуем что-нибудь этакое?
    </p>
    <p>
     Ставим на КПК Winamp, находим ссылку на потоковое радио с низким битрейтом, загружаем, и... Всё заработало, он-лайн радио играет!
    </p>
    <figure class="full-width">
     <img data-blurred="true" data-src="https://habrastorage.org/getpro/habr/upload_files/74c/713/fe8/74c713fe8709d4ee86f059e7a8d44ad0.jpg" height="4160" src="https://habrastorage.org/r/w780q1/getpro/habr/upload_files/74c/713/fe8/74c713fe8709d4ee86f059e7a8d44ad0.jpg" width="3120"/>
     <figcaption>
     </figcaption>
    </figure>
    <p>
     Первые 10 секунд всё нормально, а затем характерные ритмичные затыкания говорят о буферизации, и о том, что с медиа-потоком 32 кбит/с. (наименьший, который быстро удалось найти) линия связи справляется с трудом, надо бы еще помедленнее.
    </p>
    <p>
     Финальное видео коннекта в реальном времени с серфингом и музицированием )  Прошу простить за качество, снималось с руки телефоном.
    </p>
    <div class="tm-iframe_temp" data-src="https://embedd.srv.habr.com/iframe/63eb89cf7a8d27e0260fd585" data-style="" id="63eb89cf7a8d27e0260fd585" width="">
    </div>
    <p>
     Немного лирики.
    </p>
    <p>
     Радостно и забавно снова, через 20 лет, воспользоваться ушедшей технологией связи. Вспомнить звуки, неторопливость соединения и запасы терпения на загрузку «тяжелых» страниц, и редкая закачка больших файлов тогда обходилась без призывов к магической помощи такой-то матери )
    </p>
    <p>
     Грустно от факта того, что сегодня единственный вариант вернуть старые гаджеты в естественную среду обитания — только стать самому себе провайдером. В 2023 году — звонить некуда и не на чем. Давно нет провайдеров с модемными пулами, а сама телефонная «лапша», вместе с аналоговыми АТС, везде вырезана под ноль и заменена на оптику.
    </p>
    <p>
    </p>
   </div>
  </div>
 </div>
 <!-- -->
 <!-- -->
</div>
