<div id="post-content-body">
 <div>
  <div class="article-formatted-body article-formatted-body article-formatted-body_version-2">
   <div xmlns="http://www.w3.org/1999/xhtml">
    <figure class="float">
     <img data-blurred="true" data-src="https://habrastorage.org/webt/3m/6k/it/3m6kit6wpui9zykcze0rc6iceny.jpeg" height="auto" src="https://habrastorage.org/r/w780q1/webt/3m/6k/it/3m6kit6wpui9zykcze0rc6iceny.jpeg" width="auto"/>
     <figcaption>
     </figcaption>
    </figure>
    <p>
     Привет, Хабр!
     <br/>
     <br/>
     В данной статье расскажу как модифицировать DSLogic U2Basic (PANGO) в DSLogic Plus.
     <br/>
     <br/>
     Данная статья является обновлением статей -
     <a href="https://habr.com/ru/post/445024/" rel="noopener noreferrer nofollow">
      <strong>
       Превращаем DSLogic Basic в DSLogic Plus
      </strong>
     </a>
     <strong>
     </strong>
     и
     <strong>
     </strong>
     <a href="https://habr.com/ru/post/483496/" rel="noopener noreferrer nofollow">
      <strong>
       Превращаем DSLogic U2Basic в DSLogic Plus
      </strong>
     </a>
    </p>
    <h4>
     Предисловие
    </h4>
    <p>
     DreamSourceLab — в очередной раз решили прикрыть дыру (мод до Plus), либо SPARTAN 6 закончились...
    </p>
    <p>
     DSLogic Basic — сняты с производства.
     <br/>
     DSLogic U2Basic — сняты с производства.
     <br/>
     DSLogic U2Basic (с версией DSView v1.2.2+ на коробке) — внесены взамен, со значительными изменениями схемы и прошивки.
    </p>
    <p>
     А конкретно — Spartan 6 заменен на PANGO PGL12G, из‑за чего процесс модификации требует некоторых правок.
    </p>
    <details class="spoiler">
     <summary>
      Характеристики прежние
     </summary>
     <div class="spoiler__content">
      <figure class="">
       <img data-blurred="true" data-src="https://habrastorage.org/webt/rs/xz/ud/rsxzud3hgdyhbzjwqf-ubhg9le0.jpeg" height="auto" src="https://habrastorage.org/r/w780q1/webt/rs/xz/ud/rsxzud3hgdyhbzjwqf-ubhg9le0.jpeg" width="auto"/>
       <figcaption>
       </figcaption>
      </figure>
      <p>
      </p>
     </div>
    </details>
    <p>
     Для мода достаточно лишь заменить ОЗУ, и сменить один байт в прошивке eeprom на плате анализатора.
    </p>
    <h4>
     Приступаем к работе
    </h4>
    <p>
     <strong>
      Автор не несет ответственность за ваши неудачи, и не призывает к действию!
     </strong>
    </p>
    <p>
     <strong>
      Все что вы делаете — вы делаете на свой страх и риск!
     </strong>
    </p>
    <p>
     Понадобится:
    </p>
    <ul>
     <li>
      <p>
       Паяльник;
      </p>
     </li>
     <li>
      <p>
       Тонкий пинцет (или игла);
      </p>
     </li>
     <li>
      <p>
       Программатор с поддержкой прошивки I2C EEPROM 24**** (
       <s>
        теоретически
       </s>
       <a href="https://habr.com/ru/post/445024/#comment_20586583" rel="noopener noreferrer nofollow">
        можно прошить и без программатора
       </a>
       <s>
        , но мне было лень разбираться
       </s>
       );
      </p>
     </li>
     <li>
      <p>
       *
       <a href="https://github.com/User420t/DSL" rel="noopener noreferrer nofollow">
        На всякий случай архив с прошивками
       </a>
       (PASS: 8S]7P#cGaB/X7p‑N).
      </p>
     </li>
     <li>
      <p>
       ОЗУ AS4C16M16SA или
       <a href="https://www.alliancememory.com/wp-content/uploads/2020/02/Alliance-Memory-e-catalog2020feb.pdf" rel="noopener noreferrer nofollow">
        аналог (4 страница — аналоги)
       </a>
       ;
      </p>
     </li>
     <li>
      <p>
       Сам девайс.
      </p>
     </li>
    </ul>
    <h4>
     Вскрываем
    </h4>
    <details class="spoiler">
     <summary>
      Hidden text
     </summary>
     <div class="spoiler__content">
      <p>
       Выкручиваем 4 болтика, поддеваем крышку пинцетом или присоской.
      </p>
      <figure class="">
       <img data-blurred="true" data-src="https://habrastorage.org/webt/f_/-i/6w/f_-i6w3uzpoqmwmescyyg7yragk.jpeg" height="auto" src="https://habrastorage.org/r/w780q1/webt/f_/-i/6w/f_-i6w3uzpoqmwmescyyg7yragk.jpeg" width="auto"/>
       <figcaption>
       </figcaption>
      </figure>
      <p>
       Осторожно выполняем следующие действия:
      </p>
      <ol>
       <li>
        <p>
         Выкручиваем еще 4 болтика крепящие плату к корпусу.
        </p>
       </li>
       <li>
        <p>
         Двигаем плату в сторону разъема входов каналов.
        </p>
       </li>
       <li>
        <p>
         Слегка отгибаем плату.
        </p>
       </li>
       <li>
        <p>
         Вынимаем плату давя пальцами на разъем входов каналов.
        </p>
       </li>
      </ol>
      <figure class="">
       <img data-blurred="true" data-src="https://habrastorage.org/webt/g3/wx/yd/g3wxydn0tdf5otlkxcnf4rrthmg.jpeg" height="auto" src="https://habrastorage.org/r/w780q1/webt/g3/wx/yd/g3wxydn0tdf5otlkxcnf4rrthmg.jpeg" width="auto"/>
       <figcaption>
       </figcaption>
      </figure>
      <p>
       Фото актуальной платы:
      </p>
      <figure class="full-width">
       <img data-blurred="true" data-src="https://habrastorage.org/getpro/habr/upload_files/a6c/264/d0d/a6c264d0daeefc2bca9dc2b74d32b76c.jpg" height="1150" src="https://habrastorage.org/r/w780q1/getpro/habr/upload_files/a6c/264/d0d/a6c264d0daeefc2bca9dc2b74d32b76c.jpg" width="1134"/>
       <figcaption>
       </figcaption>
      </figure>
      <p>
      </p>
     </div>
    </details>
    <h4>
     Меняем ОЗУ, подготавливаем к прошивке
    </h4>
    <details class="spoiler">
     <summary>
      Hidden text
     </summary>
     <div class="spoiler__content">
      <p>
       На плате видим ОЗУ - выпаиваем заводскую и запаиваем правильную ОЗУ
       <strong>
        соблюдая ключ
       </strong>
       .
      </p>
      <p>
       <strong>
        Хорошенько пропаиваем контакты паяльником, не оставляем сопли!
       </strong>
      </p>
      <figure class="">
       <img data-blurred="true" data-src="https://habrastorage.org/webt/ur/ym/fy/urymfydxl7nucjtkebgzly7kksm.jpeg" height="auto" src="https://habrastorage.org/r/w780q1/webt/ur/ym/fy/urymfydxl7nucjtkebgzly7kksm.jpeg" width="auto"/>
       <figcaption>
       </figcaption>
      </figure>
      <p>
       Фото актуальной платы:
      </p>
      <figure class="full-width">
       <img data-blurred="true" data-src="https://habrastorage.org/getpro/habr/upload_files/7a0/0af/131/7a00af1319d9aaaa16895e2cb27ed946.jpg" height="1104" src="https://habrastorage.org/r/w780q1/getpro/habr/upload_files/7a0/0af/131/7a00af1319d9aaaa16895e2cb27ed946.jpg" width="1280"/>
       <figcaption>
       </figcaption>
      </figure>
      <p>
       Видим чип в 8ми ногом корпусе - это I2C EEPROM, ее нужно прошить.
      </p>
      <figure class="">
       <img data-blurred="true" data-src="https://habrastorage.org/webt/xr/js/wl/xrjswlg4sonxdtxrmgdpcsyc_ky.jpeg" height="auto" src="https://habrastorage.org/r/w780q1/webt/xr/js/wl/xrjswlg4sonxdtxrmgdpcsyc_ky.jpeg" width="auto"/>
       <figcaption>
       </figcaption>
      </figure>
      <p>
       Программатор EZP2010 прошивку не осилил (ошибка при сравнении, каждый раз на разном адресе).
      </p>
      <p>
       Потому беру программатор на основе CH341A.
      </p>
      <p>
       Чтобы не выпаивать чип - использую скрепку SOIC8 SOP8.
      </p>
      <p>
       Так как первый пин EEPROM на плате подключен к питанию, а на программаторе к земле - грею паяльником и слегка поднимаю первый пин пинцетом:
       <br/>
      </p>
      <figure class="">
       <img data-blurred="true" data-src="https://habrastorage.org/webt/tg/jg/ne/tgjgned-prx_2bbukgx6-2yyzwi.jpeg" height="auto" src="https://habrastorage.org/r/w780q1/webt/tg/jg/ne/tgjgned-prx_2bbukgx6-2yyzwi.jpeg" width="auto"/>
       <figcaption>
       </figcaption>
      </figure>
      <p>
       Потом осторожно(чтоб не придавить первый пин обратно), цепляю прищепку
       <strong>
        соблюдая ключ
       </strong>
       .
      </p>
      <p>
       И только потом подключаю программатор к ПК.
      </p>
      <p>
      </p>
     </div>
    </details>
    <h4>
     Прошивка
    </h4>
    <p>
     1) Считываем EEPROM.
    </p>
    <ol start="2">
     <li>
      <p>
       Делаем поиск по HEX запросу 0E 2A, и меняем следующий за ними байт
       <strong>
        31
       </strong>
       на
       <strong>
        30
       </strong>
       (адрес может отличаться, пример из старой статьи, внимательно читаем текст):
      </p>
     </li>
    </ol>
    <figure class="">
     <img data-blurred="true" data-src="https://habrastorage.org/webt/gu/f9/nc/guf9ncf7ovrinp0qtjck9ksehls.jpeg" height="auto" src="https://habrastorage.org/r/w780q1/webt/gu/f9/nc/guf9ncf7ovrinp0qtjck9ksehls.jpeg" width="auto"/>
     <figcaption>
     </figcaption>
    </figure>
    <figure class="">
     <img data-blurred="true" data-src="https://habrastorage.org/webt/yv/cz/u7/yvczu7gzypvizf0bijodkj2nyak.jpeg" height="auto" src="https://habrastorage.org/r/w780q1/webt/yv/cz/u7/yvczu7gzypvizf0bijodkj2nyak.jpeg" width="auto"/>
     <figcaption>
     </figcaption>
    </figure>
    <ol start="3">
     <li>
      <p>
       Прошиваем, проверяем.
      </p>
     </li>
    </ol>
    <p>
     После удачной прошивки:
    </p>
    <ol>
     <li>
      <p>
       Отключаю программатор.
      </p>
     </li>
     <li>
      <p>
       Запаиваю первый пин EEPROM чипа обратно.
      </p>
     </li>
     <li>
      <p>
       Подключаю к ПК.
      </p>
     </li>
    </ol>
    <p>
     В софте определяется так:
    </p>
    <figure class="">
     <img data-blurred="true" data-src="https://habrastorage.org/webt/da/st/kx/dastkxny3uyt38lvzv2yrh_le4c.jpeg" height="auto" src="https://habrastorage.org/r/w780q1/webt/da/st/kx/dastkxny3uyt38lvzv2yrh_le4c.jpeg" width="auto"/>
     <figcaption>
     </figcaption>
    </figure>
    <p>
     Проверяем работу на каком нибудь железе, на пример UART.
    </p>
    <p>
     Если все ОК - отключаем, отмываем спиртом, собираем.
    </p>
    <p>
     Если же данные битые - еще раз хорошенько пропаиваем озу или меняем ее.
    </p>
    <p>
     Профит!
    </p>
    <p>
     Спасибо за внимание!
    </p>
    <p>
     Так же хочу выразить отдельную благодарность юзеру
     <a class="mention" href="/users/brwnbr">
      @brwnbr
     </a>
     , благодаря которому и появилось данное обновление статьи.
    </p>
    <p>
     При копировании попрошу оставлять ссылочку на первоисточник.
    </p>
    <p>
     С вопросами обращайтесь в комментарии, чем смогу — помогу.
    </p>
    <p>
    </p>
   </div>
  </div>
 </div>
 <!-- -->
 <!-- -->
</div>
