<div id="post-content-body">
 <div>
  <div class="article-formatted-body article-formatted-body article-formatted-body_version-2">
   <div xmlns="http://www.w3.org/1999/xhtml">
    <figure class="full-width">
     <img data-src="https://habrastorage.org/getpro/habr/upload_files/44d/b9d/da9/44db9dda99bee0910e31f4c7101e9649.png" height="600" src="https://habrastorage.org/r/w1560/getpro/habr/upload_files/44d/b9d/da9/44db9dda99bee0910e31f4c7101e9649.png" width="800">
      <figcaption>
      </figcaption>
     </img>
    </figure>
    <p>
     После окончания Второй мировой войны японская экономика лежала в руинах в прямом смысле этого слова. Американские ковровые бомбардировки сравняли с землей сотни заводов и предприятий по всей стране, а последними гвоздями, забитыми в гроб некогда могущественной империи стали атомные бомбы, сброшенные на Хиросиму и Нагасаки. Миллионы людей остались без крова, к ним прибавились сотни тысяч репатриантов, выдворенных из бывших колоний.
    </p>
    <p>
     Если говорить про цифры, то промышленность производила в шесть раз меньше товаров, чем до войны, а экспорт упал в девять раз. Но уже через пару десятилетий Япония стала второй по мощи экономикой капиталистического мира и удерживала эту позицию вплоть до начала нынешнего века, когда ее сместил с пьедестала Китай, многое перенявший у своего соседа. Как поверженной стране удалось совершить то, что в последствии назовут “экономическим чудом”? И какую роль в этом сыграла интеллектуальная собственность? Об этом мы расскажем в нашем материале.
    </p>
    <h3>
     Шоковая терапия по-американски
    </h3>
    <p>
     Уже в конце 1946 года детальный план восстановления японской экономики был принят правительством. Он предлагал определить две отрасли, ключевые для восстановления всей экономики, а затем бросить туда всю дефицитную валюту. В данном случае это были угольная промышленность и металлургия. Взаимно подпитывая друг друга, они потянули бы за собой остальные отрасли. Автором проекта был профессор-экономист Токийского университета Хироми Арисава.
    </p>
    <p>
     Сталелитейная промышленность подтолкнула строительство и судостроение. Помимо металлургии огромные деньги были вложены в легкую промышленность: в производство одежды, тканей, мелких бытовых изделий. За рубежом закупались мазут, железная руда, хлопок, а на экспорт шла сперва не слишком качественная, но все-таки готовая продукция, которая давала столь нужную стране валюту.
    </p>
    <p>
     В 1949 году детройтсктий банкир Джозефа Додж положил конец первому этапу японских экономических реформ. Его назначили советником главнокомандующего оккупационными войсками союзников генерала Дугласа Макартура. В своей пресс-конференции в феврале 1949 он заявил: «Японская экономика — это человек на ходулях. Одна ходуля — американская помощь, другая — скрытые бюджетные субсидии предприятиям. Они у вас стали слишком высокие — вы упадете, и это парализует экономику».
    </p>
    <p>
     В итоге из бюджета были вычеркнуты все необеспеченные субсидии. Гиперинфляция, достигшая 200% в год, прекратилась. Но выросла безработица.
    </p>
    <p>
     Не менее кардинальной была сельскохозяйственная реформа. Другой советник Макартура, американский экономист Вольф Ладежинский, провел аграрную реформу. Помещичье землевладение было уничтожено, крестьяне стали собственниками земли. Эти меры помогли победить голод и отменить карточную систему.
    </p>
    <p>
     Существенную роль сыграл роспуск дзайбацу — финансово-промышленных групп, контролировавших японскую экономику. Им на смену пришли кэйрэцу. Роспуск дзайбацу и запрет на концентрацию экономической мощи был мерой, которая поспособствовала созданию конкурентоспособной промышленности. 18 крупных компаний было распущено. Напрмер, Japan Steel Corporation была раздроблена на Yamata Steel и Fuji Steel. Один клан у верхушки был заменен горизонтальными связями между небольшими фирмами, входящими в конгломерат (кэйрэцу).
    </p>
    <p>
     Летом 1950 года началась Корейская война, ставшая катализатором развития экономики. Япония получила огромные интендантские заказы от американской армии — на ткани, проволоку, кабели, цемент и т.д. Валюта хлынула в страну. Но доставалась она далеко не всем предприятиям.
    </p>
    <h3>
     Заимствованные  и усовершенствованные инновации
    </h3>
    <p>
     В 1950-ые годы в Японии был введен абсолютный валютный контроль со стороны государства. Вся экспортная выручка поступала на государственный валютный счет, и предприятиям выдавалась под строго оговоренные сделки — на покупку сырья для приоритетных отраслей, новейшего иностранного оборудования или лицензий. Особенно важным оказался последний аспект. В приоритете был импорт технологий, а не товаров, которые страна и сама стала производить в огромном количестве при непрерывном росте их качества.
    </p>
    <p>
     В 1950-1970-е японские компании скупили более 15 тысяч патентов и лицензий (в основном у своего заклятого друга — в Соединенных Штатах). Вместо того чтобы тратить деньги и время на исследования, скупали права на продукт, а потом усовершенствовали его. Так, американская корпорация DuPont 11 лет разрабатывала производство нейлона, потратив в общей сложности $25 млн. Японская компания Toyo Rayon попросту выкупила патент за $7,5 млн, но получила на экспорте нейлона выручку в $90 млн.
    </p>
    <p>
     Но самым ярким примером, безусловно, является корпорация Sony. Ее основатель Морита Акио полгода обивал пороги Министерства внешней торговли и промышленности (МВТП) в надежде выбить валюту на приобретение в Америке патента на транзистор. Чиновники не верили в перспективность идеи и денег не давали. Да и Sony тогда была небольшой фирмой, а не могущественной корпорацией. Но Морита все-таки настоял на своем. Патент был куплен, и над усовершенствованием транзистора стала работать исследовательская группа во главе с Лео Эсаки. Достаточно быстро ученый смог разработать туннельные диоды с шириной перехода всего в три десятка атомов, что привело к созданию генераторов и детекторов высокочастотных сигналов. Что это означало на практике?
    </p>
    <p>
     Sony и Япония стали пионерами в массовом применении транзисторов в радиоэлектронике. Мир заполонили сперва компактные радиоприемники, а затем и другие товары. За свое
     <a href="https://web.archive.org/web/20221218005634mp_/https://journals.aps.org/pr/abstract/10.1103/PhysRev.109.603">
      <u>
       открытие
      </u>
     </a>
     Лео Эсаки был удостоен Нобелевской премии по физике в 1973 году. Но куда важнее оказался научный вклад в японскую экономику, средний рост ВВП которой в 1960-х достигал 10% в год, в 1970-х — 5%, в 1980-х — 4%. Объем экспорта увеличивался еще быстрее: по 17% в год в 1960-х, 21% — в 1970-х, 11% — в 1980-х. Японский экспорт, начав с $4,1 млрд в год в 1960-м, к 1990-му подошел на отметке $287 млрд.
    </p>
    <p>
     Впоследствии по японскому пути пошли и другие азиатские страны. Самые яркие примеры — Южная Корея и Китай. Но эти кейсы заслуживают отдельных статей. А сама Япония уже в 1980-е годы выбилась в число лидеров по числу выданных патентов на изобретения. Правда, от затяжного экономического кризиса это страну не спасло.
    </p>
    <details class="spoiler">
     <summary>
      О сервисе Онлайн Патент
     </summary>
     <div class="spoiler__content">
      <p>
       Онлайн Патент – цифровая система №1 в рейтинге Роспатента. С 2013 года мы создаем уникальные LegalTech-решения для защиты и управления интеллектуальной собственностью.
       <a href="https://my.onlinepatent.ru/client/claims/list/?utm_source=habr&amp;utm_medium=organic&amp;utm_campaign=habr_organic_article64">
        Зарегистрируйтесь в сервисе Онлайн-Патент
       </a>
       и получите доступ к следующим услугам:
      </p>
      <ul>
       <li>
        <p>
         Онлайн-регистрация программ, патентов на изобретение, товарных знаков, промышленного дизайна;
        </p>
       </li>
       <li>
        <p>
         Подача заявки на внесение в реестр отечественного ПО;
        </p>
       </li>
       <li>
        <p>
         Опции ускоренного оформления услуг;
        </p>
       </li>
       <li>
        <p>
         Бесплатный поиск по базам патентов, программ, товарных знаков;
        </p>
       </li>
       <li>
        <p>
         Мониторинги новых заявок по критериям;
        </p>
       </li>
       <li>
        <p>
         Онлайн-поддержку специалистов.
        </p>
       </li>
      </ul>
      <p>
       Больше статей, аналитики от экспертов и полезной информации о интеллектуальной собственности в России и мире ищите в
       <a href="https://t.me/+DsqzUsFtJrA1OGY6">
        нашем Телеграм-канале
       </a>
       .
      </p>
      <p>
       Получите скидку в 2000 рублей на первый заказ.
       <a href="https://t.me/onlinepatent/121">
        Подробнее в закрепленном посте.
       </a>
      </p>
     </div>
    </details>
    <p>
    </p>
   </div>
  </div>
 </div>
 <!-- -->
 <!-- -->
</div>
