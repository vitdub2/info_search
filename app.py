from flask import Flask, render_template, jsonify, request
from task5 import process_query

app = Flask(__name__)


@app.route("/")
def hello_world():
    return render_template('index.html')


@app.route("/search", methods=['POST'])
def users_api():
    query = request.form['query']
    urls = process_query(query)
    return jsonify(urls)
